#ifndef VULKANAPPCONTAINER_HPP
#define VULKANAPPCONTAINER_HPP
#include "VulkanInstance.hpp"
namespace Vk {

DEFINE_CLASS(AppContainer)
class AppContainer
{
private:
	sInstance instance;
	VkSurfaceKHR surface;
	std::string app_name;
	std::string engine_name;
protected:
	virtual void initializeWindow() = 0;
public:
	AppContainer();
	void setInstance(sInstance n_instance);
	void createInstance(const void* pNextApp, const char* pApplicationName,
				uint32_t applicationVersion, const char* pEngineName, uint32_t engineVersion,
				uint32_t apiVersion, const void* pNext, VkInstanceCreateFlags flags,
				uint32_t enabledLayerCount,
				const char* const* ppEnabledLayerNames, uint32_t enabledExtensionCount,
				const char* const* ppEnabledExtensionNames, const VkAllocationCallbacks* pAllocator=NULL);
	void createInstance(const void* pNextApp, const char* pApplicationName,
				uint32_t applicationVersion, const char* pEngineName, uint32_t engineVersion,
				uint32_t apiVersion, const void* pNext, VkInstanceCreateFlags flags,
				const CStringVector& ppEnabledLayerNames,
				const CStringVector& ppEnabledExtensionNames, const VkAllocationCallbacks* pAllocator=NULL);
	virtual ~AppContainer();

	sInstance getInstance() const;
	const VkSurfaceKHR& getSurface() const;
	VkSurfaceKHR* getSurfaceAddr();

	virtual uint getWidth() const = 0;
	virtual uint getHeight() const = 0;
#ifdef NDEBUG
	static const bool enableValidationLayers = false;
#else
	static const bool enableValidationLayers = true;
#endif
	static const std::vector<const char*> validationLayers;
};

}
#endif // VULKANAPPCONTAINER_HPP
