#include "VulkanBuffer.hpp"

namespace Vk {

Buffer::Buffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, const void* addr)
{
	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = size;
	bufferInfo.usage = usage;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	if (vkCreateBuffer(getLogicalDevice(), &bufferInfo, nullptr, &buffer) != VK_SUCCESS) {
		throw std::runtime_error("failed to create buffer!");
	}

	VkMemoryRequirements memRequirements;
	vkGetBufferMemoryRequirements(getLogicalDevice(), buffer, &memRequirements);
	memory = Memory::create(memRequirements,properties);
	vkBindBufferMemory(getLogicalDevice(), buffer, memory->getDeviceMemory(), 0);
	if(addr)
	{
		memory->mapMemory(addr,(size_t)size,0);
	}
}
Buffer::~Buffer()
{
	vkDestroyBuffer(getLogicalDevice(), buffer, nullptr);
	memory.reset();
}
const VkBuffer& Buffer::getBuffer() const
{
	return buffer;
}
VkBuffer* Buffer::getBufferAddr()
{
	return &buffer;
}
const sMemory& Buffer::getMemory() const
{
	return memory;
}
void Buffer::MapMemory(const void* addr, size_t size)
{
	memory->mapMemory(addr,size,0);
}

}
