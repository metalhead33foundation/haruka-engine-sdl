TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
include(qmake_modules/findLibraryLinker.pro)
include(qmake_modules/findPhysFS.pro)
include(qmake_modules/findAssimp.pro)
include(qmake_modules/findLibX11.pro)

HEADERS += \
    global/AbstractRenderingEngine.hpp \
    global/Clock.hpp \
    global/global.hpp \
    global/IndexArray.hpp \
    global/linkedlist.hpp \
    global/Time.hpp \
    global/UploadableGpuArray.hpp \
    global/VertexArray.hpp \
    PhysFsWrappers/PhAssimp.hpp \
    PhysFsWrappers/Physfs4Cpp.hpp \
    sdl/Sdl2Wrapper.hpp \
    vk/Device/VulkanAppContainer.hpp \
    vk/Device/VulkanInstance.hpp \
    vk/Device/VulkanLogicalDevice.hpp \
    vk/Device/VulkanPhysicalDevice.hpp \
    vk/Memory/VulkanMemory.hpp \
    vk/Memory/VulkanMemoryPartition.hpp \
    vk/Pipeline/VulkanGraphicsPipeline.hpp \
    vk/Pipeline/VulkanPipelineLayout.hpp \
    vk/Pipeline/VulkanRenderPass.hpp \
    vk/Pipeline/VulkanShader.hpp \
    vk/Pipeline/VulkanShaderModule.hpp \
    vk/Pipeline/VulkanSwapChain.hpp \
    vk/AbstractVulkanEngine.hpp \
    vk/VkCommandBufferer.hpp \
    vk/VulkanArrayBuffer.hpp \
    vk/VulkanBuffer.hpp \
    vk/VulkanFramebuffer.hpp \
    vk/VulkanResource.hpp \
    vk/VulkanWrapper.hpp \
    sdl/SdlVulkanContainer.hpp \
    sdl/SdlRenderEngine.hpp \
    vk/Vertex.hpp \
    vk/Memory/VulkanMemoryAllocator.hpp

SOURCES += \
    global/AbstractRenderingEngine.cpp \
    global/Clock.cpp \
    global/IndexArray.cpp \
    global/Time.cpp \
    global/UploadableGpuArray.cpp \
    global/VertexArray.cpp \
    PhysFsWrappers/PhAssimp.cpp \
    PhysFsWrappers/Physfs4Cpp.cpp \
    sdl/Sdl2Wrapper.cpp \
    vk/Device/VulkanAppContainer.cpp \
    vk/Device/VulkanInstance.cpp \
    vk/Device/VulkanLogicalDevice.cpp \
    vk/Device/VulkanPhysicalDevice.cpp \
    vk/Memory/VulkanMemory.cpp \
    vk/Memory/VulkanMemoryPartition.cpp \
    vk/Pipeline/VulkanGraphicsPipeline.cpp \
    vk/Pipeline/VulkanPipelineLayout.cpp \
    vk/Pipeline/VulkanRenderPass.cpp \
    vk/Pipeline/VulkanShader.cpp \
    vk/Pipeline/VulkanShaderModule.cpp \
    vk/Pipeline/VulkanSwapChain.cpp \
    vk/AbstractVulkanEngine.cpp \
    vk/VkCommandBufferer.cpp \
    vk/VulkanArrayBuffer.cpp \
    vk/VulkanBuffer.cpp \
    vk/VulkanFramebuffer.cpp \
    vk/VulkanResource.cpp \
    vk/VulkanWrapper.cpp \
    main.cpp \
    sdl/SdlVulkanContainer.cpp \
    sdl/SdlRenderEngine.cpp \
    vk/Vertex.cpp \
    vk/Memory/VulkanMemoryAllocator.cpp
