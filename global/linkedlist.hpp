#ifndef LINKEDLIST_HPP
#define LINKEDLIST_HPP
#include <memory>
template <typename T> struct LinkedList
{
	typedef T type;
	typedef LinkedList* Anchor;
	T element;
	Anchor next;
	Anchor prev;
	void decouple()
	{
		if(prev) prev->next = this->next;
		if(next) next->prev = this->prev;
	}
	~LinkedList() { decouple(); }
	Anchor toTheEnd()
	{
		Anchor anchor=this;
		while(anchor->next) anchor = anchor->next;
		return anchor;
	}
	Anchor toTheStart()
	{
		Anchor anchor=this;
		while(anchor->prev) anchor = anchor->prev;
		return anchor;
	}
	void insertBeforeThis(Anchor anchor)
	{
		anchor->prev = prev;
		anchor->next = this;
		if(prev) prev->next = anchor;
		prev = anchor;
	}
	void insertAfterThis(Anchor anchor)
	{
		anchor->prev = this;
		anchor->next = next;
		if(next) next->prev = anchor;
		next = anchor;
	}
	LinkedList(const T& data, Anchor anchor=0, bool before=false) // if before is false, then it inserts after
	{
		element = data;
		prev = 0;
		next = 0;
		if(anchor)
		{
			if(before)
			{
				anchor->insertBeforeThis(this);
			}
			else
			{
				anchor->insertAfterThis(this);
			}
		}
	}
	const T& operator *() const
	{
		return element;
	}
	typedef std::shared_ptr<LinkedList> shared;
	static shared create(const T& data, shared anchor=0, bool before=false) // if before is false, then it inserts after
	{
		return shared(new LinkedList(data,anchor.get() ,before));
	}

	static void removeAll(Anchor anchor)
	{
		Anchor head = anchor->toTheStart();
		Anchor temp;
		while(head)
		{
			temp = head;
			if(temp) head = temp->next;
			//head->prev = 0;
			//temp->next = 0;
			delete temp;
		}
	}
};

#endif // LINKEDLIST_HPP
