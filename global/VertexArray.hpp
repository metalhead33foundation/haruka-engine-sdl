#ifndef VERTEXARRAY_HPP
#define VERTEXARRAY_HPP
#include "UploadableGpuArray.hpp"
#include <vector>
#include <SFML/System/Vector3.hpp>
#include <glm/vec3.hpp> // glm::vec3


class VertexArray : public UploadableGpuArray
{
private:
	std::vector<float> mVertices;
public:
	void AddVertices(float vertex);
	void AddVertices(float x, float y, float z);
	void AddVertices(const float* vertices, size_t num);
	void AddVertices(std::vector<float>& vectrexes);
	void AddVertices(sf::Vector3f vect);
	void AddVertices(const sf::Vector3f* vertices, size_t num);
	void AddVertices(std::vector<sf::Vector3f>& vector_vector);
	void AddVertices(glm::vec3 vect);
	void AddVertices(const glm::vec3* vertices, size_t num);
	void AddVertices(std::vector<glm::vec3>& vector_vector);
	VertexArray();
	VertexArray(VertexArray* copier); // Copy constructor
	VertexArray(const float* vertices, size_t num);
	VertexArray(std::vector<float>& vectrexes);
	VertexArray(const sf::Vector3f* vertices, size_t num);
	VertexArray(std::vector<sf::Vector3f>& vector_vector);
	VertexArray(const glm::vec3* vertices, size_t num);
	VertexArray(std::vector<glm::vec3>& vector_vector);
	size_t GetArraySize() const;
	size_t GetIndividualSize() const;
	void* GetInitial();
protected:
	void CleanArray();
};

#endif // VERTEXARRAY_HPP
