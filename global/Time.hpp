#ifndef TIME_HPP
#define TIME_HPP
#include <cstdint>

class STime
{
private:
	uint64_t miliseconds;
public:
	uint64_t getMiliseconds() const;
	double getSeconds() const;
	void setMiliseconds(uint64_t setto);
	void setSeconds(double setto);
	STime(const STime& copy); // Copy constructor
	STime(uint64_t miliseconds);
	STime(double seconds);
	STime(); // Default constructor

	bool operator==(STime& b);
	bool operator==(uint64_t miliseconds);
	bool operator==(double seconds);
	bool operator!=(STime& b);
	bool operator!=(uint64_t miliseconds);
	bool operator!=(double seconds);
	bool operator<(STime& b);
	bool operator<(uint64_t miliseconds);
	bool operator<(double seconds);
	bool operator>(STime& b);
	bool operator>(uint64_t miliseconds);
	bool operator>(double seconds);
	bool operator<=(STime& b);
	bool operator<=(uint64_t miliseconds);
	bool operator<=(double seconds);
	bool operator>=(STime& b);
	bool operator>=(uint64_t miliseconds);
	bool operator>=(double seconds);
	STime operator+(STime& b);
	STime operator+(uint64_t miliseconds);
	STime operator+(double seconds);
	STime operator-(STime& b);
	STime operator-(uint64_t miliseconds);
	STime operator-(double seconds);
	STime operator*(uint64_t miliseconds);
	STime operator*(double seconds);
	STime operator/(uint64_t miliseconds);
	STime operator/(double seconds);
	STime operator%(STime& b);
	STime operator%(uint64_t miliseconds);
	void operator+=(STime& b);
	void operator+=(uint64_t miliseconds);
	void operator+=(double seconds);
	void operator-=(STime& b);
	void operator-=(uint64_t miliseconds);
	void operator-=(double seconds);
	void operator*=(uint64_t miliseconds);
	void operator*=(double seconds);
	void operator/=(uint64_t miliseconds);
	void operator/=(double seconds);
	void operator%=(STime& b);
	void operator%=(uint64_t miliseconds);
};

/*bool operator==(Time& a, Time& b);
bool operator==(Time& a, uint64_t miliseconds);
bool operator==(Time& a, double seconds);
bool operator!=(Time& a, Time& b);
bool operator!=(Time& a, uint64_t miliseconds);
bool operator!=(Time& a, double seconds);
bool operator<(Time& a, Time& b);
bool operator<(Time& a, uint64_t miliseconds);
bool operator<(Time& a, double seconds);
bool operator>(Time& a, Time& b);
bool operator>(Time& a, uint64_t miliseconds);
bool operator>(Time& a, double seconds);
bool operator<=(Time& a, Time& b);
bool operator<=(Time& a, uint64_t miliseconds);
bool operator<=(Time& a, double seconds);
bool operator>=(Time& a, Time& b);
bool operator>=(Time& a, uint64_t miliseconds);
bool operator>=(Time& a, double seconds);
Time operator+(Time& a, Time& b);
Time operator+(Time& a, uint64_t miliseconds);
Time operator+(Time& a, double seconds);
Time operator-(Time& a, Time& b);
Time operator-(Time& a, uint64_t miliseconds);
Time operator-(Time& a, double seconds);
Time operator*(Time& a, uint64_t miliseconds);
Time operator*(Time& a, double seconds);
Time operator/(Time& a, uint64_t miliseconds);
Time operator/(Time& a, double seconds);
Time operator%(Time& a, Time& b);
Time operator%(Time& a, uint64_t miliseconds);
Time& operator+=(Time& a, Time& b);
Time& operator+=(Time& a, uint64_t miliseconds);
Time& operator+=(Time& a, double seconds);
Time& operator-=(Time& a, Time& b);
Time& operator-=(Time& a, uint64_t miliseconds);
Time& operator-=(Time& a, double seconds);
Time& operator*=(Time& a, uint64_t miliseconds);
Time& operator*=(Time& a, double seconds);
Time& operator/=(Time& a, uint64_t miliseconds);
Time& operator/=(Time& a, double seconds);
Time& operator%=(Time& a, Time& b);
Time& operator%=(Time& a, uint64_t miliseconds);*/

#endif // TIME_HPP
