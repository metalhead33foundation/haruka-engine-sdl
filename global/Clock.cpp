#include "Clock.hpp"

Clock::Clock()
{
	start = SDL2::GetTicks();
}
STime Clock::getElapsed()
{
	return STime(static_cast<uint64_t>(SDL2::GetTicks() - start));
}
STime Clock::restart()
{
	STime temp(static_cast<uint64_t>(SDL2::GetTicks() - start));
	start = SDL2::GetTicks();
	return temp;
}
