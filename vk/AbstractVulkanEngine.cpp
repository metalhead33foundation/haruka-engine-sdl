#include "AbstractVulkanEngine.hpp"

namespace Vk {

AbstractEngine::AbstractEngine()
{

}
void AbstractEngine::createSemaphores()
{
	VkSemaphoreCreateInfo semaphoreInfo = {};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	if (vkCreateSemaphore(Resource::getLogicalDevice(), &semaphoreInfo, nullptr, &imageAvailableSemaphore) != VK_SUCCESS ||
	vkCreateSemaphore(Resource::getLogicalDevice(), &semaphoreInfo, nullptr, &renderFinishedSemaphore) != VK_SUCCESS) {

		throw std::runtime_error("Failed to create semaphores!");
	}
}
void AbstractEngine::destroySemaphores()
{
	vkDestroySemaphore(Resource::getLogicalDevice(), renderFinishedSemaphore, nullptr);
	vkDestroySemaphore(Resource::getLogicalDevice(), imageAvailableSemaphore, nullptr);
}
void AbstractEngine::drawFrame()
{
	uint32_t imageIndex;
	vkAcquireNextImageKHR(Resource::getLogicalDevice(), swapChain->getSwapChain(), std::numeric_limits<uint64_t>::max(), imageAvailableSemaphore, VK_NULL_HANDLE, &imageIndex);

	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

	VkSemaphore waitSemaphores[] = {imageAvailableSemaphore};
	VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = waitSemaphores;
	submitInfo.pWaitDstStageMask = waitStages;

	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commands->getCommandBuffers()[imageIndex];

	VkSemaphore signalSemaphores[] = {renderFinishedSemaphore};
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = signalSemaphores;

	if (vkQueueSubmit(Resource::getSingleton()->getGraphicsQueue(), 1, &submitInfo, VK_NULL_HANDLE) != VK_SUCCESS) {
			throw std::runtime_error("failed to submit draw command buffer!");
	}

	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = signalSemaphores;

	VkSwapchainKHR swapChains[] = {swapChain->getSwapChain() };
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = swapChains;

	presentInfo.pImageIndices = &imageIndex;

	vkQueuePresentKHR(Resource::getSingleton()->getPresentQueue(), &presentInfo);
	vkQueueWaitIdle(Resource::getSingleton()->getPresentQueue());
}
void AbstractEngine::InitializeVulkan(sAppContainer new_container)
{
	container = new_container;
	Vk::Resource::setSingleton( Vk::LogicalDevice::create(container.get() ) );
	swapChain = Vk::SwapChain::create(container.get() );
	sShader shaders = Shader::create();
	shaders->addShaderModule("vert.spv",VK_SHADER_STAGE_VERTEX_BIT);
	shaders->addShaderModule("frag.spv",VK_SHADER_STAGE_FRAGMENT_BIT);
	shaders->stage("main");
	pipeline = GraphicsPipeline::create(swapChain,shaders);
	framebuffers.resize(swapChain->getSwapChainImageViews().size());
	for (size_t i = 0; i < framebuffers.size(); i++)
	{
		framebuffers[i] = Framebuffer::create(pipeline->getRenderPass(),&swapChain->getSwapChainImageViews()[i],1,swapChain);
	}
	commands = CommandBufferer::create(container.get(),swapChain,pipeline,framebuffers);
	createSemaphores();
}
void AbstractEngine::DeinitializeVulkan()
{
	destroySemaphores();
	commands.reset();
	framebuffers.clear();
	pipeline.reset();
	swapChain.reset();
	Vk::Resource::destroySingleton();
	container.reset();
}
sAppContainer AbstractEngine::getContainer() const
{
	return container;
}

}
