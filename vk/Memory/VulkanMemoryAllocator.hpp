#ifndef VULKANMEMORYALLOCATOR_HPP
#define VULKANMEMORYALLOCATOR_HPP
#include "VulkanMemory.hpp"
#include "VulkanMemoryPartition.hpp"
namespace Vk {

class VulkanMemoryAllocator
{
private:
	Memory memSpace;
	MemoryPartition memorySystem;
public:
	VulkanMemoryAllocator(const VkMemoryRequirements& memRequirements, VkMemoryPropertyFlags properties, const void* addr=0);
	size_t measureOffsetUntil(MemoryPartition::pChunk chunkAddr);
	MemoryPosition getPosition(MemoryPartition::pChunk chunkAddr);
	MemoryPartition::pChunk claimMemory(size_t size);
	void absorbFreed(MemoryPartition::pChunk chunkAddr);
	void free(MemoryPartition::pChunk chunkAddr);
	VkDeviceSize getRoundedSize(VkDeviceSize intendedSize);

	const bool isMapped() const;
	void waitUntilFreed();
	void* startMapping(size_t size, size_t offset);
	void* startMapping(MemoryPosition& pos);
	void* startMapping(MemoryPartition::pChunk pos);
	void endMapping();
};

}
#endif // VULKANMEMORYALLOCATOR_HPP
