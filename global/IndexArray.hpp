#ifndef INDEXARRAY_HPP
#define INDEXARRAY_HPP
#include "UploadableGpuArray.hpp"
#include <vector>

class IndexArray : public UploadableGpuArray
{
public:
	typedef std::vector<unsigned int> IndexVector;
private:
	IndexVector indices;
protected:
	void CleanArray();
public:
	size_t GetArraySize() const;
	size_t GetIndividualSize() const;
	void* GetInitial();

	void UploadIndices(unsigned int index);
	void UploadIndices(unsigned int* new_indices, size_t size);
	void UploadIndices(IndexVector& new_index);

	IndexArray();
	IndexArray(IndexArray* copier); // Copy constructor
	IndexArray(unsigned int* new_indices, size_t size);
	IndexArray(IndexVector& new_index);
};

#endif // INDEXARRAY_HPP
