#ifndef VULKANRENDERPASS_HPP
#define VULKANRENDERPASS_HPP
#include "VulkanSwapChain.hpp"
namespace Vk {

DEFINE_CLASS(RenderPass)
class RenderPass : public Resource
{
private:
	VkRenderPass renderPass;
public:
	RenderPass(sSwapChain swapchain);
	~RenderPass();
	static sRenderPass create(sSwapChain swapchain);
	const VkRenderPass& getRenderPass() const;
	VkRenderPass* getRenderPassAddr();
};

}
#endif // VULKANRENDERPASS_HPP
