#include <iostream>
//#include "vk/SdlWrapper.hpp"
#include "sdl/SdlRenderEngine.hpp"

using namespace std;
const std::string appname = "Test App";
#include "PhysFsWrappers/Physfs4Cpp.hpp"

int main(int argc, char *argv[])
{
	//try {
	PhysFs::FileHandle::Initialize(argv[0]);
	InitVulkan();
	LoadSDL2();
	std::ifstream infile("mountlist.txt");
	std::string line;
	while (std::getline(infile, line))
	{
		PhysFs::FileHandle::Mount(line);
	}
	PhysFs::FileHandle::Mount(PhysFs::FileHandle::GetBaseDir() + PhysFs::FileHandle::GetDirSeparator() + "data" );

	SDL2::Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);
	SDL2::SdlRenderEngine app(1360,768,SDL_WINDOW_FULLSCREEN | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_BORDERLESS | SDL_WINDOW_INPUT_GRABBED,appname,VK_MAKE_VERSION(0,0,0));
	app.Run();

	PhysFs::FileHandle::Deinitialize();
	SDL2::Quit();
	OffloadSDL2();
	/*}
	catch(std::exception e)
	{
		std::cout << "Exception caught: " << e.what() << std::endl;
	}*/
	return 0;
}
