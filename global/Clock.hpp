#ifndef CLOCK_HPP
#define CLOCK_HPP
#include "Time.hpp"
#include "../sdl/Sdl2Wrapper.hpp"

class Clock
{
private:
	uint32_t start;
public:
	Clock();
	STime getElapsed();
	STime restart();
};

#endif // CLOCK_HPP
