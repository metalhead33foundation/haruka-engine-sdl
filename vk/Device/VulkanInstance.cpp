#include "VulkanInstance.hpp"

namespace Vk {

Instance::Instance(const VkInstanceCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator)
{
	m_callback = pAllocator;
	IF_DEBUG(
		std::cout << "Vk::Instance[" << static_cast<void*>(this) << "]: Creating Vulkan Instance" << std::endl;
	)
	VkResult res = vkCreateInstance(pCreateInfo,m_callback,&m_instance);
	if(res  != VK_SUCCESS)
	{
		if(res == VK_ERROR_INCOMPATIBLE_DRIVER) throw std::runtime_error("Failed to create Vulkan Instance! - Incompatible Vulkan Driver error.");
		else throw std::runtime_error("Failed to create Vulkan Instance! - Unknown error.");
	}
}
sInstance Instance::create(const VkInstanceCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator)
{
	return sInstance(new Instance(pCreateInfo,pAllocator));
}
sInstance Instance::create(const void* pNext, VkInstanceCreateFlags flags,
						const VkApplicationInfo* pApplicationInfo, uint32_t enabledLayerCount,
						const char* const* ppEnabledLayerNames, uint32_t enabledExtensionCount,
						const char* const* ppEnabledExtensionNames, const VkAllocationCallbacks* pAllocator)
{
	VkInstanceCreateInfo temp;
	temp.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	temp.pNext = pNext;
	temp.flags = flags;
	temp.pApplicationInfo = pApplicationInfo;
	temp.enabledLayerCount = enabledLayerCount;
	temp.ppEnabledLayerNames = ppEnabledLayerNames;
	temp.enabledExtensionCount = enabledExtensionCount;
	temp.ppEnabledExtensionNames = ppEnabledExtensionNames;
	IF_DEBUG(
		std::cout << "Instance Creation Struct Information:" << std::endl;
			std::cout << "pType: " << temp.sType << std::endl;
			std::cout << "flags: " << temp.flags << std::endl;
	)
	return create(&temp, pAllocator);
}

sInstance Instance::create(const void* pNext, VkInstanceCreateFlags flags,
						const VkApplicationInfo* pApplicationInfo, const CStringVector& ppEnabledLayerNames,
						const CStringVector& ppEnabledExtensionNames, const VkAllocationCallbacks* pAllocator)
{
	VkInstanceCreateInfo temp;
	temp.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	temp.pNext = pNext;
	temp.flags = flags;
	temp.pApplicationInfo = pApplicationInfo;
	temp.enabledLayerCount = ppEnabledLayerNames.size();
	temp.ppEnabledLayerNames = ppEnabledLayerNames.data();
	temp.enabledExtensionCount = ppEnabledExtensionNames.size();
	temp.ppEnabledExtensionNames = ppEnabledExtensionNames.data();
	return create(&temp, pAllocator);
}

sInstance Instance::create(const void* pNextApp, const char* pApplicationName,
			uint32_t applicationVersion, const char* pEngineName, uint32_t engineVersion,
			uint32_t apiVersion, const void* pNext, VkInstanceCreateFlags flags,
			uint32_t enabledLayerCount, const char* const* ppEnabledLayerNames, uint32_t enabledExtensionCount,
			const char* const* ppEnabledExtensionNames, const VkAllocationCallbacks* pAllocator)
{
	VkApplicationInfo temp;
	temp.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	temp.pNext = pNextApp;
	temp.pApplicationName = pApplicationName;
	temp.applicationVersion = applicationVersion;
	temp.pEngineName = pEngineName;
	temp.engineVersion = engineVersion;
	temp.apiVersion = apiVersion;
	VkInstanceCreateInfo temp2;
	temp2.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	temp2.pNext = pNext;
	temp2.flags = flags;
	temp2.pApplicationInfo = &temp;
	temp2.enabledLayerCount = enabledLayerCount;
	temp2.ppEnabledLayerNames = ppEnabledLayerNames;
	temp2.enabledExtensionCount = enabledExtensionCount;
	temp2.ppEnabledExtensionNames = ppEnabledExtensionNames;
	return create(&temp2, pAllocator);
}
sInstance Instance::create(const void* pNextApp, const char* pApplicationName,
			uint32_t applicationVersion, const char* pEngineName, uint32_t engineVersion,
			uint32_t apiVersion, const void* pNext, VkInstanceCreateFlags flags,
			const CStringVector& ppEnabledLayerNames,
			const CStringVector& ppEnabledExtensionNames, const VkAllocationCallbacks* pAllocator)
{
	VkApplicationInfo temp;
	temp.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	temp.pNext = pNextApp;
	temp.pApplicationName = pApplicationName;
	temp.applicationVersion = applicationVersion;
	temp.pEngineName = pEngineName;
	temp.engineVersion = engineVersion;
	temp.apiVersion = apiVersion;
	VkInstanceCreateInfo temp2;
	temp2.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	temp2.pNext = pNext;
	temp2.flags = flags;
	temp2.pApplicationInfo = &temp;
	temp2.enabledLayerCount = ppEnabledLayerNames.size();
	temp2.ppEnabledLayerNames = ppEnabledLayerNames.data();
	temp2.enabledExtensionCount = ppEnabledExtensionNames.size();
	temp2.ppEnabledExtensionNames = ppEnabledExtensionNames.data();
	return create(&temp2, pAllocator);
}

Instance::~Instance()
{
	IF_DEBUG(
		std::cout << "Vk::Instance[" << static_cast<void*>(this) << "]: Destroying instance." << std::endl;
	)
	vkDestroyInstance(m_instance,m_callback);
	IF_DEBUG(
		std::cout << "Vk::Instance[" << static_cast<void*>(this) << "]: Succesfully destroyed instance." << std::endl;
	)
}
const VkInstance& Instance::getInstance() const
{
	return m_instance;
}
VkInstance* Instance::getInstanceAddr()
{
	return &m_instance;
}

}
