#ifndef GLOBAL_HPP
#define GLOBAL_HPP
#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <stdexcept>
#include <algorithm>
#include <cstdlib>
#include <cstring>
// From util.h - Written by Daniël Sonck
#include <memory>
#define DEBUG
// #define VK_USE_PLATFORM_XLIB_KHR
#define VK_USE_PLATFORM_XCB_KHR
typedef std::vector<const char*> CStringVector;
typedef CStringVector::iterator CStringIterator;

#define DEFINE_PTR(a) typedef a *p##a; \
   typedef std::shared_ptr< a > s##a; \
   typedef std::weak_ptr< a > w##a;

#define DEFINE_PTR2(s,a) typedef s::a *p##a; \
   typedef std::shared_ptr< s::a > s##a; \
   typedef std::weak_ptr< s::a > w##a;

#define DEFINE_CLASS(klass) class klass; DEFINE_PTR(klass)

//End of former util.h
// Some useful defines
#define ESZ(elem) (int)elem.size()

#if defined(NDEBUG) || defined(DEBUG) || defined(_DEBUG) || defined(DEBUG_BUILD)
#   define IF_DEBUG( ... )   __VA_ARGS__
#else
#   define IF_DEBUG( ... )
#endif /* DEBUG */

#include "linkedlist.hpp"

#endif // GLOBAL_HPP
