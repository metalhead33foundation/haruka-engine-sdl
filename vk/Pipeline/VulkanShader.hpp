#ifndef VULKANSHADER_HPP
#define VULKANSHADER_HPP
#include "VulkanShaderModule.hpp"
namespace Vk {

DEFINE_CLASS(Shader)
class Shader
{
public:
	typedef std::pair<sShaderModule,VkShaderStageFlagBits> unstagedModule;
	typedef std::vector<unstagedModule> ModuleVector;
	typedef std::vector<VkPipelineShaderStageCreateInfo> StageVector;
	typedef ModuleVector::iterator ModuleIterator;
	typedef StageVector::iterator StageIterator;
private:
	ModuleVector unstagedModules;
	StageVector stagedModules;
	bool staged;
	Shader();
public:
	void addShaderModule(const std::vector<char>& code,VkShaderStageFlagBits stage);
	void addShaderModule(std::string path,VkShaderStageFlagBits stage);
	void addShaderModule(sShaderModule module, VkShaderStageFlagBits stage);
	void addShaderModule(unstagedModule& module);

	void stage(const char* name);
	void stage(const std::string& name);
	void clear();
	bool isStaged() const;
	size_t getStageCount();
	VkPipelineShaderStageCreateInfo* getStages();
	static sShader create();
};

}
#endif // VULKANSHADER_HPP
