#include "SdlVulkanContainer.hpp"
#ifdef VK_USE_PLATFORM_XCB_KHR
#include <X11/Xlib-xcb.h> /* for XGetXCBConnection() */
#endif
namespace SDL2
{

void SdlVulkanContainer::initializeWindow()
{
	IF_DEBUG(
		std::cout << "SDL2::SdlVulkanContainer[" << static_cast<void*>(this) << "]: Initializing window." << std::endl;
		std::cout << "SDL Window address: [" << static_cast<void*>(m_window) << "].\n";
	)
#if defined(VK_USE_PLATFORM_WIN32_KHR)
	VkWin32SurfaceCreateInfoKHR surface_create_info;
	memset(&surface_create_info, 0, sizeof(surface_create_info));
	surface_create_info.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
	surface_create_info.hinstance = GetModuleHandle(NULL);
	surface_create_info.hwnd = sys_wm_info.info.win.window;
	VkResult result = vkCreateWin32SurfaceKHR(getInstance()->getInstance(), &surface_create_info, NULL, getSurfaceAddr());
#elif defined(VK_USE_PLATFORM_XCB_KHR)
	VkXcbSurfaceCreateInfoKHR vkWindowInfo;
	vkWindowInfo.sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR;
	vkWindowInfo.pNext = 0;
	vkWindowInfo.flags = 0;
	//SDL_VERSION(&sdlWindowInfo.info);
	vkWindowInfo.window = sdlWindowInfo.info.x11.window;
	IF_DEBUG(
		std::cout << "X11 Display address: [" << static_cast<void*>(sdlWindowInfo.info.x11.display) << "].\n";
	)
	vkWindowInfo.connection = XGetXCBConnection(sdlWindowInfo.info.x11.display);
	//vkWindowInfo.connection = XGetXCBConnection((Display*)sdlWindowInfo.info.x11.display);
	VkResult result = vkCreateXcbSurfaceKHR(getInstance()->getInstance(),&vkWindowInfo,0,getSurfaceAddr());
#elif defined(VK_USE_PLATFORM_XLIB_KHR)
	VkXlibSurfaceCreateInfoKHR vkWindowInfo;
	vkWindowInfo.sType = VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR;
	vkWindowInfo.pNext = 0;
	vkWindowInfo.flags = 0;
	vkWindowInfo.window = sdlWindowInfo.info.x11.window;
	vkWindowInfo.dpy = sdlWindowInfo.info.x11.display;
	IF_DEBUG(
		std::cout << "X11 Display address: [" << static_cast<void*>(sdlWindowInfo.info.x11.display) << "].\n";
	)
	VkResult result = vkCreateXlibSurfaceKHR(getInstance()->getInstance(),&vkWindowInfo,0,getSurfaceAddr());
#elif defined(VK_USE_PLATFORM_WAYLAND_KHR)
	VkWaylandSurfaceCreateInfoKHR vkWindowInfo;
	vkWindowInfo.sType = VK_STRUCTURE_TYPE_WAYLAND_SURFACE_CREATE_INFO_KHR;
	vkWindowInfo.pNext = 0;
	vkWindowInfo.flags = 0;
	vkWindowInfo.display = sdlWindowInfo.info.wl.display;
	vkWindowInfo.surface = sdlWindowInfo.info.wl.surface;
	VkResult result = vkCreateWaylandSurfaceKHR(getInstance()->getInstance(),&vkWindowInfo,0,getSurfaceAddr());
#elif defined(VK_USE_PLATFORM_MIR_KHR)
	VkMirSurfaceCreateInfoKHR vkWindowInfo;
	vkWindowInfo.sType = VK_STRUCTURE_TYPE_MIR_SURFACE_CREATE_INFO_KHR;
	vkWindowInfo.pNext = 0;
	vkWindowInfo.flags = 0;
	vkWindowInfo.connection = sdlWindowInfo.info.mir.connection;
	vkWindowInfo.mirSurface = sdlWindowInfo.info.mir.surface;
	VkResult result = vkCreateMirSurfaceKHR(getInstance()->getInstance(),&vkWindowInfo,0,getSurfaceAddr());
#elif defined(VK_USE_PLATFORM_ANDROID_KHR)
	VkAndroidSurfaceCreateInfoKHR vkWindowInfo;
	vkWindowInfo.sType = VK_STRUCTURE_TYPE_ANDROID_SURFACE_CREATE_INFO_KHR;
	vkWindowInfo.pNext = 0;
	vkWindowInfo.flags = 0;
	vkWindowInfo.window = sdlWindowInfo.info.android.window;
	VkResult result = vkCreateAndroidSurfaceKHR(getInstance()->getInstance(),&vkWindowInfo,0,getSurfaceAddr());
#endif
	if(result != VK_SUCCESS)
	{
		switch(result)
		{
		case VK_ERROR_OUT_OF_HOST_MEMORY :
			{
				throw std::runtime_error("Couldn't create the window surface - out of host memory!");
				break;
			}
		case VK_ERROR_OUT_OF_DEVICE_MEMORY :
			{
				throw std::runtime_error("Couldn't create the window surface - out of device memory!");
				break;
			}
		default:
			{
				throw std::runtime_error("Couldn't create the window surface!");
				break;
			}
		}
	}
	IF_DEBUG(
		std::cout << "SDL2::SdlVulkanContainer[" << static_cast<void*>(this) << "]: Successfully initialized window." << std::endl;
	)
}

SdlVulkanContainer::SdlVulkanContainer(uint width, uint height, Uint32 sdlFlags, const std::string& engine_name, uint32_t engine_version, const std::string& appname,
									   uint32_t app_version, CStringVector& desiredExtensions)
{
	h = height;
	w = width;
	CStringVector extensionVector;
	/*const char** extensions = glfwGetRequiredInstanceExtensions(&extension_count);
	for(uint32_t i = 0;i < extension_count;++i)
	{
		extensionVector.push_back(extensions[i]);
	}*/
	extensionVector.push_back("VK_KHR_surface");
#if defined(VK_USE_PLATFORM_WIN32_KHR)
	extensionVector.push_back("VK_KHR_win32_surface");
#elif defined(VK_USE_PLATFORM_XCB_KHR)
	extensionVector.push_back("VK_KHR_xcb_surface");
#elif defined(VK_USE_PLATFORM_XLIB_KHR)
	extensionVector.push_back("VK_KHR_xlib_surface");
#elif defined(VK_USE_PLATFORM_WAYLAND_KHR)
	extensionVector.push_back("VK_KHR_wayland_surface");
#elif defined(VK_USE_PLATFORM_MIR_KHR)
	extensionVector.push_back("VK_KHR_mir_surface");
#elif defined(VK_USE_PLATFORM_ANDROID_KHR)
	extensionVector.push_back("VK_KHR_android_surface");
#endif
	for(CStringIterator it = desiredExtensions.begin();it != desiredExtensions.end();++it)
	{
		extensionVector.push_back(*it);
	}
	CStringVector layerVector;
	createInstance(NULL, appname.c_str(),app_version, engine_name.c_str() , engine_version,
						VK_API_VERSION_1_0, NULL, 0,validationLayers,extensionVector,NULL);
	m_window = SDL2::CreateWindow(appname.c_str(),
								  SDL_WINDOWPOS_UNDEFINED,
								  SDL_WINDOWPOS_UNDEFINED,
								  width, height, sdlFlags);
	if(!m_window) throw std::runtime_error("Failed to create window!");
	SDL_VERSION(&sdlWindowInfo.version);
	SDL2::GetWindowWMInfo(m_window, &sdlWindowInfo);
	initializeWindow();
}
SdlVulkanContainer::~SdlVulkanContainer()
{
	SDL2::DestroyWindow(m_window);
}

sSdlVulkanContainer SdlVulkanContainer::create(uint width, uint height, Uint32 sdlFlags, const std::string& engine_name, uint32_t engine_version,
										 const std::string& appname,uint32_t app_version, CStringVector& desiredExtensions)
{
	return sSdlVulkanContainer(new SdlVulkanContainer(width, height, sdlFlags, engine_name, engine_version,appname,app_version,
												 desiredExtensions));
}
Vk::pAppContainer SdlVulkanContainer::castBack()
{
	return static_cast<Vk::pAppContainer>(this);
}
Vk::sAppContainer SdlVulkanContainer::createCast(uint width, uint height, Uint32 sdlFlags, const std::string& engine_name, uint32_t engine_version, const std::string& appname,
uint32_t app_version, CStringVector& desiredExtensions)
{
	return std::dynamic_pointer_cast<Vk::AppContainer>(create(width, height, sdlFlags, engine_name, engine_version,appname,app_version,
													 desiredExtensions));
}

}
