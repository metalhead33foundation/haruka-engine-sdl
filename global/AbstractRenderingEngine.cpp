#include "AbstractRenderingEngine.hpp"
#include <stdexcept>
#include <iostream>

AbstractRenderingEngine::AbstractRenderingEngine()
{
	m_clock = 0;
	exit_signal = false;
}

void AbstractRenderingEngine::Run()
{
	exit_signal = false;
	if(!Initialize()) throw std::runtime_error("Unable to initialize!");
	m_clock = new Clock();
	IF_DEBUG(
	Clock seconds;
	int frames=0;
	)
	do
	{
		Render(m_clock->restart());
		IF_DEBUG(
		if(seconds.getElapsed() >= (uint64_t)1000)
		{
			std::cout << "FPS: " << frames << std::endl;
			seconds.restart();
			frames = 0;
		}
		else
		{
			++frames;
		}
		)
	} while(!exit_signal);
	delete m_clock;
	Deinitialize();
}
