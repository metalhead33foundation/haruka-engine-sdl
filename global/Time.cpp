#include "Time.hpp"

uint64_t STime::getMiliseconds() const
{
	return miliseconds;
}
double STime::getSeconds() const
{
	return static_cast<double>(miliseconds / 1000.00);
}
void STime::setMiliseconds(uint64_t setto)
{
	miliseconds = setto;
}
void STime::setSeconds(double setto)
{
	miliseconds = static_cast<uint64_t>(setto * 1000.00);
}
STime::STime()
{
	this->miliseconds = 0;
}
STime::STime(uint64_t miliseconds)
{
	this->miliseconds = miliseconds;
}
STime::STime(double seconds)
{
	this->miliseconds = static_cast<uint64_t>(seconds * 1000.00);
}
STime::STime(const STime& copy)
{
	this->miliseconds = copy.miliseconds;
}

bool STime::operator==(STime& b)
{
	return this->miliseconds == b.miliseconds;
}

bool STime::operator==(uint64_t miliseconds)
{
	return this->miliseconds == miliseconds;
}

bool STime::operator==(double seconds)
{
	return getSeconds() == seconds;
}

bool STime::operator!=(STime& b)
{
	return this->miliseconds != b.miliseconds;
}

bool STime::operator!=(uint64_t miliseconds)
{
	return this->miliseconds != miliseconds;
}

bool STime::operator!=(double seconds)
{
	return getSeconds() != seconds;
}

bool STime::operator<(STime& b)
{
	return this->miliseconds < b.miliseconds;
}

bool STime::operator<(uint64_t miliseconds)
{
	return this->miliseconds < miliseconds;
}

bool STime::operator<(double seconds)
{
	return getSeconds() < seconds;
}

bool STime::operator>(STime& b)
{
	return this->miliseconds > b.miliseconds;
}

bool STime::operator>(uint64_t miliseconds)
{
	return this->miliseconds > miliseconds;
}

bool STime::operator>(double seconds)
{
	return getSeconds() > seconds;
}

bool STime::operator<=(STime& b)
{
	return this->miliseconds <= b.miliseconds;
}

bool STime::operator<=(uint64_t miliseconds)
{
	return this->miliseconds <= miliseconds;
}

bool STime::operator<=(double seconds)
{
	return getSeconds() <= seconds;
}

bool STime::operator>=(STime& b)
{
	return this->miliseconds >= b.miliseconds;
}

bool STime::operator>=(uint64_t miliseconds)
{
	return this->miliseconds >= miliseconds;
}

bool STime::operator>=(double seconds)
{
	return getSeconds() >= seconds;
}

STime STime::operator+(STime& b)
{
	return STime(this->miliseconds + b.miliseconds);
}

STime STime::operator+(uint64_t miliseconds)
{
	return STime(this->miliseconds + miliseconds);
}

STime STime::operator+(double seconds)
{
	return STime(getSeconds() + seconds);
}

STime STime::operator-(STime& b)
{
	return STime(this->miliseconds - b.miliseconds);
}

STime STime::operator-(uint64_t miliseconds)
{
	return STime(this->miliseconds - miliseconds);
}

STime STime::operator-(double seconds)
{
	return STime(getSeconds() - seconds);
}

STime STime::operator*(uint64_t miliseconds)
{
	return STime(this->miliseconds * miliseconds);
}

STime STime::operator*(double seconds)
{
	return STime(getSeconds() * seconds);
}

STime STime::operator/(uint64_t miliseconds)
{
	return STime(this->miliseconds / miliseconds);
}

STime STime::operator/(double seconds)
{
	return STime(getSeconds() / seconds);
}

STime STime::operator%(STime& b)
{
	return STime(this->miliseconds % b.miliseconds);
}

STime STime::operator%(uint64_t miliseconds)
{
	return STime(this->miliseconds % miliseconds);
}

void STime::operator+=(STime& b)
{
	this->miliseconds += b.miliseconds;
}

void STime::operator+=(uint64_t miliseconds)
{
	this->miliseconds += miliseconds;
}

void STime::operator+=(double seconds)
{
	setSeconds(getSeconds() + seconds);
}

void STime::operator-=(STime& b)
{
	this->miliseconds -= b.miliseconds;
}

void STime::operator-=(uint64_t miliseconds)
{
	this->miliseconds -= miliseconds;
}

void STime::operator-=(double seconds)
{
	setSeconds(getSeconds() - seconds);
}

void STime::operator*=(uint64_t miliseconds)
{
	this->miliseconds *= miliseconds;
}

void STime::operator*=(double seconds)
{
	setSeconds(getSeconds() * seconds);
}

void STime::operator/=(uint64_t miliseconds)
{
	this->miliseconds /= miliseconds;
}

void STime::operator/=(double seconds)
{
	setSeconds(getSeconds() / seconds);
}

void STime::operator%=(STime& b)
{
	this->miliseconds %= b.miliseconds;
}

void STime::operator%=(uint64_t miliseconds)
{
	this->miliseconds %= miliseconds;
}


/*
bool operator==(Time& a, Time& b)
{
	return a.getMiliseconds() == b.getMiliseconds();
}

bool operator==(Time& a, uint64_t miliseconds)
{
	return a.getMiliseconds() == miliseconds;
}

bool operator==(Time& a, double seconds)
{
	return a.getSeconds() == seconds;
}

bool operator!=(Time& a, Time& b)
{
	return a.getMiliseconds() != b.getMiliseconds();
}

bool operator!=(Time& a, uint64_t miliseconds)
{
	return a.getMiliseconds() != miliseconds;
}

bool operator!=(Time& a, double seconds)
{
	return a.getSeconds() != seconds;
}

bool operator<(Time& a, Time& b)
{
	return a.getMiliseconds() < b.getMiliseconds();
}

bool operator<(Time& a, uint64_t miliseconds)
{
	return a.getMiliseconds() < miliseconds;
}

bool operator<(Time& a, double seconds)
{
	return a.getSeconds() < seconds;
}

bool operator>(Time& a, Time& b)
{
	return a.getMiliseconds() > b.getMiliseconds();
}

bool operator>(Time& a, uint64_t miliseconds)
{
	return a.getMiliseconds() > miliseconds;
}

bool operator>(Time& a, double seconds)
{
	return a.getSeconds() > seconds;
}

bool operator<=(Time& a, Time& b)
{
	return a.getMiliseconds() <= b.getMiliseconds();
}

bool operator<=(Time& a, uint64_t miliseconds)
{
	return a.getMiliseconds() <= miliseconds;
}

bool operator<=(Time& a, double seconds)
{
	return a.getSeconds() <= seconds;
}

bool operator>=(Time& a, Time& b)
{
	return a.getMiliseconds() >= b.getMiliseconds();
}

bool operator>=(Time& a, uint64_t miliseconds)
{
	return a.getMiliseconds() >= miliseconds;
}

bool operator>=(Time& a, double seconds)
{
	return a.getSeconds() >= seconds;
}

Time operator+(Time& a, Time& b)
{
	return Time(a.getMiliseconds() + b.getMiliseconds() );
}

Time operator+(Time& a, uint64_t miliseconds)
{
	return Time(a.getMiliseconds() + miliseconds);
}

Time operator+(Time& a, double seconds)
{
	return Time(a.getSeconds() + seconds);
}

Time operator-(Time& a, Time& b)
{
	return Time(a.getMiliseconds() - b.getMiliseconds() );
}

Time operator-(Time& a, uint64_t miliseconds)
{
	return Time(a.getMiliseconds() - miliseconds);
}

Time operator-(Time& a, double seconds)
{
	return Time(a.getSeconds() - seconds);
}

Time operator*(Time& a, uint64_t miliseconds)
{
	return Time(a.getMiliseconds() * miliseconds);
}

Time operator*(Time& a, double seconds)
{
	return Time(a.getSeconds() * seconds);
}

Time operator/(Time& a, uint64_t miliseconds)
{
	return Time(a.getMiliseconds() / miliseconds);
}

Time operator/(Time& a, double seconds)
{
	return Time(a.getSeconds() / seconds);
}

Time operator%(Time& a, Time& b)
{
	return Time(a.getMiliseconds() % b.getMiliseconds() );
}

Time operator%(Time& a, uint64_t miliseconds)
{
	return Time(a.getMiliseconds() % miliseconds);
}

Time& operator+=(Time& a, Time& b)
{
	a.setMiliseconds(a.getMiliseconds() + b.getMiliseconds());
	return a;
}

Time& operator+=(Time& a, uint64_t miliseconds)
{
	a.setMiliseconds(a.getMiliseconds() + miliseconds);
	return a;
}

Time& operator+=(Time& a, double seconds)
{
	a.setSeconds(a.getSeconds() + seconds);
	return a;
}

Time& operator-=(Time& a, Time& b)
{
	a.setMiliseconds(a.getMiliseconds() - b.getMiliseconds());
	return a;
}

Time& operator-=(Time& a, uint64_t miliseconds)
{
	a.setMiliseconds(a.getMiliseconds() - miliseconds);
	return a;
}

Time& operator-=(Time& a, double seconds)
{
	a.setSeconds(a.getSeconds() - seconds);
	return a;
}

Time& operator*=(Time& a, uint64_t miliseconds)
{
	a.setMiliseconds(a.getMiliseconds() * miliseconds);
	return a;
}

Time& operator*=(Time& a, double seconds)
{
	a.setSeconds(a.getSeconds() * seconds);
	return a;
}

Time& operator/=(Time& a, uint64_t miliseconds)
{
	a.setMiliseconds(a.getMiliseconds() / miliseconds);
	return a;
}

Time& operator/=(Time& a, double seconds)
{
	a.setSeconds(a.getSeconds() / seconds);
	return a;
}

Time& operator%=(Time& a, Time& b)
{
	a.setMiliseconds(a.getMiliseconds() % b.getMiliseconds());
	return a;
}

Time& operator%=(Time& a, uint64_t miliseconds)
{
	a.setMiliseconds(a.getMiliseconds() % miliseconds);
	return a;
}
*/
