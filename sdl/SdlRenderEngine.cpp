#include "SdlRenderEngine.hpp"
namespace SDL2
{

const std::string SdlRenderEngine::ENGINE_NAME = "Haruka Engine";
const uint32_t SdlRenderEngine::ENGINE_VERSION = VK_MAKE_VERSION(1,0,0);

SdlRenderEngine::SdlRenderEngine(uint x, uint y, Uint32 sdlFlags, const std::string& name, uint32_t version)
{
	width = x;
	height = y;
	appname = name;
	app_version = version;
	flags = sdlFlags;
}
bool SdlRenderEngine::Initialize()
{
	//glfwInit();
	CStringVector desired_extensions(0);
	InitializeVulkan( SdlVulkanContainer::createCast(width,height,flags,ENGINE_NAME,ENGINE_VERSION,appname,app_version,desired_extensions) );
	return true;
}
void SdlRenderEngine::Render(STime deltaTime)
{
	SDL_Event ev;
	while (SDL2::PollEvent(&ev))
	{
	switch (ev.type)
	{
		case SDL_WINDOWEVENT:
		{
			switch(ev.window.event)
			{
				case SDL_WINDOWEVENT_CLOSE:
					exit_signal = true;
					break;
				default:
					break;
			}
		}
		case SDL_KEYDOWN:
		{
			switch(ev.key.keysym.sym)
			{
			case SDLK_ESCAPE: exit_signal = true; break;
			default: break;
			}
		}
	default:
		break;
	}
	//exit_signal = true;
	}
	if(!exit_signal)
	{
		//glfwPollEvents();
		drawFrame();
	}
}
void SdlRenderEngine::Deinitialize()
{
	DeinitializeVulkan();
	//container.reset();
	// glfwTerminate();
}

}
