#include "VertexArray.hpp"
#include <stdexcept>

VertexArray::VertexArray()
	: UploadableGpuArray()
{
	;
}
VertexArray::VertexArray(VertexArray* copier)
	: UploadableGpuArray()
{
	if(copier->GetLocdown()) throw std::runtime_error("Cannot copy from an already uploaded-to-GPU vertex array.");
	mVertices = copier->mVertices;
} // Copy constructor
VertexArray::VertexArray(const float* vertices, size_t num)
	: UploadableGpuArray()
{
	AddVertices(vertices,num);
}
VertexArray::VertexArray(std::vector<float>& vectrexes)
	: UploadableGpuArray()
{
	AddVertices(vectrexes);
}
VertexArray::VertexArray(const sf::Vector3f* vertices, size_t num)
	: UploadableGpuArray()
{
	AddVertices(vertices,num);
}
VertexArray::VertexArray(std::vector<sf::Vector3f>& vector_vector)
	: UploadableGpuArray()
{
	AddVertices(vector_vector);
}
VertexArray::VertexArray(const glm::vec3* vertices, size_t num)
	: UploadableGpuArray()
{
	AddVertices(vertices,num);
}
VertexArray::VertexArray(std::vector<glm::vec3>& vector_vector)
	: UploadableGpuArray()
{
	AddVertices(vector_vector);
}
size_t VertexArray::GetArraySize() const
{
	return mVertices.size();
}
size_t VertexArray::GetIndividualSize() const
{
	return sizeof(float);
}

void VertexArray::AddVertices(float vertex)
{
	if(GetLocdown()) throw std::runtime_error("Cannot upload vertices to the list, because you have already uploaded the date to the GPU.");
	mVertices.push_back(vertex);
}
void VertexArray::AddVertices(float x, float y, float z)
{
	AddVertices(x);
	AddVertices(y);
	AddVertices(z);
}
void VertexArray::AddVertices(const float* vertices, size_t num)
{
	for(size_t i = 0;i < num;++i) AddVertices(vertices[i]);
}
void VertexArray::AddVertices(std::vector<float>& vectrexes)
{
	for(std::vector<float>::iterator it = vectrexes.begin();it != vectrexes.end();++it)
		AddVertices(*it);
}
void VertexArray::AddVertices(sf::Vector3f vect)
{
	AddVertices(vect.x);
	AddVertices(vect.y);
	AddVertices(vect.z);
}
void VertexArray::AddVertices(const sf::Vector3f* vertices, size_t num)
{
	for(size_t i = 0;i < num;++i) AddVertices(vertices[i]);
}
void VertexArray::AddVertices(std::vector<sf::Vector3f>& vector_vector)
{
	for(std::vector<sf::Vector3f>::iterator it = vector_vector.begin();it != vector_vector.end();++it)
		AddVertices(*it);
}
void VertexArray::AddVertices(glm::vec3 vect)
{
	AddVertices(vect.x);
	AddVertices(vect.y);
	AddVertices(vect.z);
}
void VertexArray::AddVertices(const glm::vec3* vertices, size_t num)
{
	for(size_t i = 0;i < num;++i) AddVertices(vertices[i]);
}
void VertexArray::AddVertices(std::vector<glm::vec3>& vector_vector)
{
	for(std::vector<glm::vec3>::iterator it = vector_vector.begin();it != vector_vector.end();++it)
		AddVertices(*it);
}
void VertexArray::CleanArray()
{
	mVertices.clear();
}
void* VertexArray::GetInitial()
{
	return &mVertices[0];
}
