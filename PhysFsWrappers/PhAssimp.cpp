#include "PhAssimp.hpp"

const std::string PhassimpStream::ReadMode = "r";
const std::string PhassimpStream::WriteMode = "w";
const std::string PhassimpStream::Append = "a";

PhassimpStream::PhassimpStream(std::string name, Mode mode)
	: PhysFs::FileHandle(name,mode)
{
	;
}

PhassimpStream::PhassimpStream(std::string name, std::string mode)
	: PhysFs::FileHandle(name,moder(mode))
{
	;
}
PhassimpStream::~PhassimpStream(void)
{
	;
}

PhysFs::FileHandle::Mode PhassimpStream::moder(std::string stringer)
{
	if(stringer.find(ReadMode) != std::string::npos) return PhysFs::FileHandle::READ;
	else if(stringer.find(WriteMode) != std::string::npos) return PhysFs::FileHandle::WRITE;
	else if(stringer.find(Append) != std::string::npos) return PhysFs::FileHandle::APPEND;
	else return PhysFs::FileHandle::READ;
}

size_t PhassimpStream::Read( void* pvBuffer, size_t pSize, size_t pCount)
{
	return pRead(pvBuffer,pSize,pCount);
}
size_t PhassimpStream::Write( const void* pvBuffer, size_t pSize, size_t pCount)
{
	return pWrite(pvBuffer,pSize,pCount);
}
aiReturn PhassimpStream::Seek( size_t pOffset, aiOrigin pOrigin)
{
	switch(pOrigin)
	{
		case aiOrigin_SET:
			pSeek(pOffset);
			break;
		case aiOrigin_CUR:
			pSeek(pTell() + pOffset);
			break;
		case aiOrigin_END:
			pSeek(pFileLength() - pOffset);
			break;
	default:
		return aiReturn_FAILURE;
	}
	return aiReturn_SUCCESS;
}
size_t PhassimpStream::Tell() const
{
	return pTell();
}
size_t PhassimpStream::FileSize() const
{
	return pFileLength();
}
void PhassimpStream::Flush()
{
	pFlush();
}

bool PhassimpSystem::Exists( const char* pFile) const
{
	return PhysFs::FileHandle::Exists(pFile);
}
char PhassimpSystem::getOsSeparator() const
{
	return PhysFs::FileHandle::GetDirSeparator().c_str()[0];
}
Assimp::IOStream* PhassimpSystem::Open( const char* pFile, const char* pMode)
{
	return new PhassimpStream(pFile,pMode);
}
void PhassimpSystem::Close( Assimp::IOStream* pFile)
{
	PhassimpStream* caster = dynamic_cast<PhassimpStream*>(pFile);
	delete caster;
}
