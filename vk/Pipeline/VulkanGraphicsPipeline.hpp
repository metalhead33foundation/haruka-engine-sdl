#ifndef VULKANGRAPHICSPIPELINE_HPP
#define VULKANGRAPHICSPIPELINE_HPP

#include "VulkanPipelineLayout.hpp"
#include "VulkanShader.hpp"
#include "VulkanRenderPass.hpp"

namespace Vk {

DEFINE_CLASS(GraphicsPipeline)
class GraphicsPipeline : public Resource
{
private:
	VkPipeline graphicsPipeline;
	sPipelineLayout layout;
	sRenderPass renderPass;
public:
	GraphicsPipeline(sSwapChain swapchain, sShader shader);
	~GraphicsPipeline();
	const VkPipeline& getPipeline() const;
	VkPipeline* getPipelineAddr();
	sPipelineLayout getLayout() const;
	sRenderPass getRenderPass() const;
	static sGraphicsPipeline create(sSwapChain swapchain, sShader shader);
};

}

#endif // VULKANGRAPHICSPIPELINE_HPP
