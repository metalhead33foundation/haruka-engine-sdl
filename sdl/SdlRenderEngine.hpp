#ifndef SDLRENDERENGINE_HPP
#define SDLRENDERENGINE_HPP
#include "SdlVulkanContainer.hpp"
#include "../global/AbstractRenderingEngine.hpp"
#include "../vk/AbstractVulkanEngine.hpp"
namespace SDL2
{

class SdlRenderEngine : public AbstractRenderingEngine, public Vk::AbstractEngine
{
private:
	uint width;
	uint height;
	std::string appname;
	uint32_t app_version;
	Uint32 flags;
public:
	SdlRenderEngine(uint x, uint y, Uint32 sdlFlags, const std::string& name, uint32_t version);
	virtual bool Initialize();
	virtual void Render(STime deltaTime);
	virtual void Deinitialize();
	static const std::string ENGINE_NAME;
	static const uint32_t ENGINE_VERSION;
};

}
#endif // SDLRENDERENGINE_HPP
