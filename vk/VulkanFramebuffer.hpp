#ifndef VULKANFRAMEBUFFER_HPP
#define VULKANFRAMEBUFFER_HPP
#include "Pipeline/VulkanRenderPass.hpp"
#include "Pipeline/VulkanSwapChain.hpp"
namespace Vk {

DEFINE_CLASS(Framebuffer)
typedef std::vector<sFramebuffer> FramebufferVector;
typedef FramebufferVector::iterator FramebufferIterator;
class Framebuffer : public Resource
{
private:
	VkFramebuffer frameBuffer;
public:
	const VkFramebuffer& getFramebuffer() const;
	VkFramebuffer* getFramebufferAddr();
	Framebuffer(sRenderPass renderPass,const VkImageView* attachments,uint32_t attachmentCount,uint32_t width,uint32_t height);
	~Framebuffer();
	static sFramebuffer create(sRenderPass renderPass,const VkImageView* attachments,uint32_t attachmentCount,uint32_t width,uint32_t height);
	static sFramebuffer create(sRenderPass renderPass,const VkImageView* attachment,uint32_t width,uint32_t height);
	static sFramebuffer create(sRenderPass renderPass,const std::vector<VkImageView>& attachments,uint32_t width,uint32_t height);
	static sFramebuffer create(sRenderPass renderPass,const VkImageView* attachments,uint32_t attachmentCount,sSwapChain swapchain);
	static sFramebuffer create(sRenderPass renderPass,const VkImageView* attachment,sSwapChain swapchain);
	static sFramebuffer create(sRenderPass renderPass,const std::vector<VkImageView>& attachments,sSwapChain swapchain);
};

}

#endif // VULKANFRAMEBUFFER_HPP
