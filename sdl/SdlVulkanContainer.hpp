#ifndef SDLVULKANCONTAINER_HPP
#define SDLVULKANCONTAINER_HPP
#include "Sdl2Wrapper.hpp"
#include "../vk/Device/VulkanAppContainer.hpp"
namespace SDL2
{

DEFINE_CLASS(SdlVulkanContainer)
class SdlVulkanContainer : public Vk::AppContainer
{
private:
	SDL_SysWMinfo sdlWindowInfo;
	SDL_Window* m_window;
	int w;
	int h;
protected:
	void initializeWindow();
public:
	SdlVulkanContainer(uint width, uint height, Uint32 sdlFlags, const std::string& engine_name, uint32_t engine_version, const std::string& appname,
					   uint32_t app_version, CStringVector& desiredExtensions);
	~SdlVulkanContainer();
	uint getWidth() const { return w; }
	uint getHeight() const { return h; }
	SDL_Window* getWindow() const { return m_window; }
	static sSdlVulkanContainer create(uint width, uint height, Uint32 sdlFlags, const std::string& engine_name, uint32_t engine_version, const std::string& appname,
									  uint32_t app_version, CStringVector& desiredExtensions);

	Vk::pAppContainer castBack();
	static Vk::sAppContainer createCast(uint width, uint height, Uint32 sdlFlags, const std::string& engine_name, uint32_t engine_version, const std::string& appname,
	uint32_t app_version, CStringVector& desiredExtensions);
};

}
#endif // SDLVULKANCONTAINER_HPP
