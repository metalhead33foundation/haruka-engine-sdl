#ifndef PHASSIMP_HPP
#define PHASSIMP_HPP
#include <assimp/IOStream.hpp>
#include <assimp/IOSystem.hpp>
#include "Physfs4Cpp.hpp"

// Foward-declaring PhysSystem
class PhassimpSystem;

// My own implementation of IOStream
class PhassimpStream : public Assimp::IOStream, public PhysFs::FileHandle
{
  friend class PhassimpSystem;
protected:
  // Constructor protected for private usage by MyIOSystem
  PhassimpStream(std::string name, Mode mode);
  PhassimpStream(std::string name, std::string mode);
public:
  static Mode moder(std::string stringer);
  static const std::string ReadMode;
  static const std::string WriteMode;
  static const std::string Append;
  ~PhassimpStream(void);
  size_t Read( void* pvBuffer, size_t pSize, size_t pCount);
  size_t Write( const void* pvBuffer, size_t pSize, size_t pCount);
  aiReturn Seek( size_t pOffset, aiOrigin pOrigin);
  size_t Tell() const;
  size_t FileSize() const;
  void Flush ();
};

// Fisher Price - My First Filesystem
class PhassimpSystem : public Assimp::IOSystem
{
public:
  PhassimpSystem() {;}
  ~PhassimpSystem() {;}
  // Check whether a specific file exists
  bool Exists( const char* pFile) const;
  // Get the path delimiter character we'd like to see
  char getOsSeparator() const;
  // ... and finally a method to open a custom stream
  Assimp::IOStream* Open( const char* pFile, const char* pMode);
  void Close( Assimp::IOStream* pFile);
};
#endif // PHASSIMP_HPP
