#include "VulkanMemoryPartition.hpp"
namespace Vk {

MemoryFile::MemoryFile(bool first, std::size_t second)
{
	free = first;
	size = second;
}
MemoryPosition::MemoryPosition(std::size_t first, std::size_t second)
{
	size = first;
	offset = second;
}
MemoryFile::MemoryFile()
{
	free = false;
	size = 0;
}
MemoryPosition::MemoryPosition()
{
	size = 0;
	offset = 0;
}

MemoryPartition::MemoryPartition(std::size_t size)
{
	first_chunk = new Chunk(MemoryFile(true,size));
}
MemoryPartition::~MemoryPartition()
{
	Chunk::removeAll(first_chunk);
}
void MemoryPartition::refreshFirstChunk()
{
	first_chunk = first_chunk->toTheStart();
}
size_t MemoryPartition::measureOffsetUntil(pChunk chunkAddr)
{
	size_t temp=0;
	pChunk head = first_chunk;
	// refreshFirstChunk();
	while(head && head != chunkAddr)
	{
		temp += head->element.size;
		head = head->next;
	}
	return temp;
}
MemoryPosition MemoryPartition::getPosition(pChunk chunkAddr)
{
	MemoryPosition temp(0,0);
	temp.size = chunkAddr->element.size;
	pChunk head = first_chunk;
	// refreshFirstChunk();
	while(head && head != chunkAddr)
	{
		temp.offset += head->element.size;
		head = head->next;
	}
	return temp;
}
/*void* MemoryPartition::mapChunk(pChunk chunkAddr)
{
	if(!chunkAddr) return 0;
	size_t offset = measureOffsetUntil(chunkAddr);
	memspace->waitUntilFreed();
	return memspace->startMapping(chunkAddr->element.second,offset);
}
void MemoryPartition::unmapChunk()
{
	memspace->endMapping();
}*/
MemoryPartition::pChunk MemoryPartition::claimMemory(size_t size)
{
	if(!first_chunk)
	{
		return new Chunk(MemoryFile(false,size));
	}
	pChunk head = first_chunk;
	while(head)
	{
		if(head->element.free && head->element.size >= size)
		{
			size_t remainder = head->element.size - size;
			if(!remainder)
			{
				head->element.free = false;
				return head;
			}
			else
			{
				pChunk temp = new Chunk(MemoryFile(false,size),head,true);
				head->element.size = remainder;
				if(head == first_chunk) first_chunk = temp;
				return temp;
			}
		}
		head = head->next;
	}
	head = first_chunk->toTheEnd();
	return new Chunk(MemoryFile(false,size),head,false);
}
void MemoryPartition::mergeWithPrev(pChunk chunkAddr)
{
	if(!chunkAddr) return;
	if(!chunkAddr->prev) return;
	if(chunkAddr->prev == first_chunk) first_chunk = chunkAddr;
	chunkAddr->element.size += chunkAddr->prev->element.size;
	delete chunkAddr->prev;
}
void MemoryPartition::mergeWithNext(pChunk chunkAddr)
{
	if(!chunkAddr) return;
	if(!chunkAddr->next) return;
	chunkAddr->element.size += chunkAddr->next->element.size;
	delete chunkAddr->next;
}
void MemoryPartition::absorbFreed(pChunk chunkAddr)
{
	if(!chunkAddr) return;
	if(!chunkAddr->element.free) return;
	while(chunkAddr->prev)
	{
		if(chunkAddr->prev->element.free) mergeWithPrev(chunkAddr);
		else break;
	}
	while(chunkAddr->next)
	{
		if(chunkAddr->next->element.free) mergeWithNext(chunkAddr);
		else break;
	}
	if(!chunkAddr->next) delete chunkAddr;
}

}
