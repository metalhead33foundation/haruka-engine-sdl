#ifndef VERTEX_HPP
#define VERTEX_HPP
#include <glm/glm.hpp>
#include <array>
#include "VulkanWrapper.hpp"
struct Vertex {
	glm::vec2 pos;
	glm::vec3 color;
	static VkVertexInputBindingDescription getBindingDescription();
	static std::array<VkVertexInputAttributeDescription, 2> getAttributeDescriptions();
};


#endif // VERTEX_H
