#include "VulkanArrayBuffer.hpp"
#include <cstring>

namespace Vk {

ArrayBuffer::ArrayBuffer(VkBufferUsageFlagBits n_flag, UploadableGpuArray* n_buff)
{
	this->flag = n_flag;
	this->m_buff = n_buff;
}
ArrayBuffer::~ArrayBuffer()
{
	if(m_buff->GetLocdown())
	{
		vkDestroyBuffer(getLogicalDevice(), iBufferObject, nullptr);
		iBufferMemory.reset();
	}
}
const VkBuffer& ArrayBuffer::GetBufferObject() const
{
	return iBufferObject;
}
sMemory ArrayBuffer::getMemory() const
{
	return iBufferMemory;
}
void ArrayBuffer::OffloadToBuffer()
{
	size = m_buff->GetFullSize();
	elems = m_buff->GetArraySize();
	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = m_buff->GetFullSize();
	bufferInfo.usage = flag;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	if (vkCreateBuffer(getLogicalDevice(), &bufferInfo, nullptr, &iBufferObject) != VK_SUCCESS) {
		throw std::runtime_error("Failed to create array buffer!");
	}
	VkMemoryRequirements memRequirements;
	vkGetBufferMemoryRequirements(getLogicalDevice(), iBufferObject, &memRequirements);
	iBufferMemory = Memory::create(memRequirements,0);
	vkBindBufferMemory(getLogicalDevice(), iBufferObject, iBufferMemory->getDeviceMemory(), 0);
	iBufferMemory->mapMemory(m_buff->GetInitial(), size,0); // Need to set up offset later
}
void ArrayBuffer::GenerateAllInOne()
{
	OffloadToBuffer();
	m_buff->Upload();
}
const VkBufferUsageFlagBits& ArrayBuffer::getFlag() const
{
	return flag;
}
size_t ArrayBuffer::GetElemnum() const
{
	return elems;
}

}
