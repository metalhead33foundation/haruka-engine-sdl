#include "VulkanShader.hpp"
namespace Vk {

Shader::Shader()
{
	staged = false;
}
void Shader::stage(const char* name)
{
	if(staged) throw std::runtime_error("You have already staged!");
	for(ModuleIterator it = unstagedModules.begin();it != unstagedModules.end();++it)
	{
		stagedModules.push_back( (*it).first->stage( (*it).second,name  ) );
		//(*it).first.reset();
	}
	//unstagedModules.clear();
	staged = true;
}
void Shader::stage(const std::string& name)
{
	stage(name.c_str());
}
void Shader::addShaderModule(const std::vector<char>& code,VkShaderStageFlagBits stage)
{
	if(staged) throw std::runtime_error("Cannot add new unstaged module after having staged the shader already!");
	else addShaderModule(ShaderModule::create(code),stage);
}
void Shader::addShaderModule(std::string path,VkShaderStageFlagBits stage)
{
	if(staged) throw std::runtime_error("Cannot add new unstaged module after having staged the shader already!");
	else addShaderModule(ShaderModule::create(path),stage);
}
void Shader::addShaderModule(sShaderModule module, VkShaderStageFlagBits stage)
{
	unstagedModule temp = unstagedModule(module,stage);
	addShaderModule(temp);
}
void Shader::addShaderModule(unstagedModule& module)
{
	if(staged) throw std::runtime_error("Cannot add new unstaged module after having staged the shader already!");
	else unstagedModules.push_back(module);
}

void Shader::clear()
{
	for(ModuleIterator it = unstagedModules.begin();it != unstagedModules.end();++it)
	{
		(*it).first.reset();
	}
	unstagedModules.clear();
}
size_t Shader::getStageCount()
{
	if(staged) return stagedModules.size();
	else return 0;
}
VkPipelineShaderStageCreateInfo* Shader::getStages()
{
	if(staged) return stagedModules.data();
	else return 0;
}
sShader Shader::create()
{
	return sShader(new Shader());
}
bool Shader::isStaged() const
{
	return staged;
}

}
